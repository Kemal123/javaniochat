package niochat;

import java.net.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;


class Client {

    private Socket socket;
    private BufferedReader in; // поток чтения из сокета
    private BufferedWriter out; // поток чтения в сокет
    private BufferedReader inputUser; // поток чтения с консоли
    private String addr; // ip адрес клиента
    private int port; // порт соединения
    private String nickname; // имя клиента
    private Date time;
    private String dtime;
    private SimpleDateFormat dt1;


    public Client(String addr, int port) {
        this.addr = addr;
        this.port = port;
        try {
            this.socket = new Socket(addr, port);
        } catch (IOException e) {
            System.err.println("Socket failed");
        }
        try {
            inputUser = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.pressNickname(); // перед началом необходимо спросит имя
            new ReadMsg().start(); // нить читающая сообщения из сокета в бесконечном цикле
            new WriteMsg().start(); // нить пишущая сообщения в сокет приходящие с консоли в бесконечном цикле
        } catch (IOException e) {
            Client.this.downService();
        }

    }


    private void pressNickname() {
        System.out.print("Введите свой ник: ");
        try {
            nickname = inputUser.readLine();
            out.write("Добро пожаловать в чат " + nickname + " ;)" + "\n");
            out.flush();
        } catch (IOException ignored) {
        }

    }


    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {
        }
    }

    // нить чтения сообщений с сервера
    private class ReadMsg extends Thread {
        @Override
        public void run() {

            String str;
            try {
                while (true) {
                    str = in.readLine(); // ждем сообщения с сервера
                    if (str.equals("stop")) {
                        Client.this.downService();
                        break;
                    }
                    System.out.println(str);
                }
            } catch (IOException e) {
                Client.this.downService();
            }
        }
    }

    // нить отправляющая сообщения приходящие с консоли на сервер
    public class WriteMsg extends Thread {

        @Override
        public void run() {
            while (true) {
                String userWord;
                try {
                    time = new Date();
                    dt1 = new SimpleDateFormat("HH:mm:ss");
                    dtime = dt1.format(time); // время
                    userWord = inputUser.readLine(); // сообщения с консоли
                    if (userWord.equals("stop")) {
                        out.write("stop" + "\n");
                        Client.this.downService();
                        break;
                    } else {
                        out.write("(" + dtime + ") " + nickname + ": " + userWord + "\n"); // отправляем на сервер
                    }
                    out.flush();
                } catch (IOException e) {
                    Client.this.downService();

                }

            }
        }
    }
}




// ДЛЯ ЗАПУСКА КЛИЕНТОВ_____________________________________________________

class ConnectionClient {

    public static String ipAddr = "localhost";
    public static int port = 8080;


    public static void main(String[] args) {
        new Client(ipAddr, port);
    }
}
class ConnectionClient1 {

    public static String ipAddr = "localhost";
    public static int port = 8080;


    public static void main(String[] args) {
        new Client(ipAddr, port);
    }
}

class ConnectionClient2 {

    public static String ipAddr = "localhost";
    public static int port = 8080;


    public static void main(String[] args) {
        new Client(ipAddr, port);
    }
}
